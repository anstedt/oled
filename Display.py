#!/usr/bin/env python3

from DisplayController import DisplayController
import time

import OLED

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

class Display():
  def __init__(self):
    # The I2C address of the OLED display, its width and height
    self.control = DisplayController(OLED.SSD1306_I2C_ADDRESS, 128, 32)
    print("Address =", self.control.address)

    self.control.begin()
    self.control.clear()
    self.control.display()

  def draw(self):
    width = self.control.width
    height = self.control.height
    image = Image.new('1', (width, height))

    # Get drawing object to draw on image.
    draw = ImageDraw.Draw(image)

    # Load default font.
    font = ImageFont.load_default()

    # Draw a black filled box to clear the image.
    draw.rectangle((0,0,width,height), outline=0, fill=0)

    draw.text((0, 0), "Howard J Anstedt", font=font, fill=255)

    # Display image.
    self.control.image(image)
    self.control.display()
    
if __name__ == '__main__':
  display = Display()
  print("Initializing/Drawing")
  display.draw()
  time.sleep(10)
  try:
    while True:
      print("Stopping Scroll")
      display.control.scroll_stop()
      print("Ready To Scroll")
      time.sleep(2)
      display.control.scroll()
      print("Scrolling")
      time.sleep(10)

  except KeyboardInterrupt:
    print("STOPPED")
    display.control.scroll_stop()
    display.control.clear()
