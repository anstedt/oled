#!/usr/bin/env python3

import struct
import sys

import OLED

import pigpio # http://abyz.co.uk/rpi/pigpio/python.html

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

if sys.version > '3':
   buffer = memoryview

class DisplayController:
  def __init__(self, address, width, height):
    self.pi = pigpio.pi()
    
    self.address = address
    self.width = width
    self.height = height

    # Bus 1 is used on RP4: i2cdetect -y 1
    self._hw = self.pi.i2c_open(1, self.address)

    self._pages = height//8
    self._buffer = [0]*(width*self._pages)

    print(self.width, self.height, self._pages, self._buffer)

    # HJA DEBUG
    #for i in range(0, 128):
      #self._buffer[i] = 0xff

    # HJA DEBUG
    # for i in range(0, len(self._buffer), 16):
      # control = 0x40   # Co = 0, DC = 0
      # self.pi.i2c_write_block_data(self.hw.control, self._buffer[i:i+16])
      
    print("Address =", hex(self.address))

  def begin(self, vccstate=OLED.SSD1306_SWITCHCAPVCC):
    """Initialize display."""
    # Save vcc state.
    self._vccstate = vccstate
    ## HJA only used fo spi Reset and initialize display.
    # self.reset()
    self._initialize()
    # Turn on the display.
    self.command(OLED.SSD1306_DISPLAYON)

  def _initialize(self):
    # 128x32 pixel specific initialization.
    self.command(OLED.SSD1306_DISPLAYOFF)                    # 0xAE
    self.command(OLED.SSD1306_SETDISPLAYCLOCKDIV)            # 0xD5
    self.command(0x80)                                  # the suggested ratio 0x80
    self.command(OLED.SSD1306_SETMULTIPLEX)                  # 0xA8
    self.command(0x1F)
    self.command(OLED.SSD1306_SETDISPLAYOFFSET)              # 0xD3
    self.command(0x0)                                   # no offset
    self.command(OLED.SSD1306_SETSTARTLINE | 0x0)            # line #0
    self.command(OLED.SSD1306_CHARGEPUMP)                    # 0x8D
    if self._vccstate == OLED.SSD1306_EXTERNALVCC:
      self.command(0x10)
    else:
      self.command(0x14)
    self.command(OLED.SSD1306_MEMORYMODE)                    # 0x20
    self.command(0x00)                                  # 0x0 act like ks0108
    self.command(OLED.SSD1306_SEGREMAP | 0x1)
    self.command(OLED.SSD1306_COMSCANDEC)
    self.command(OLED.SSD1306_SETCOMPINS)                    # 0xDA
    self.command(0x02)
    self.command(OLED.SSD1306_SETCONTRAST)                   # 0x81
    self.command(0x8F)
    self.command(OLED.SSD1306_SETPRECHARGE)                  # 0xd9
    if self._vccstate == OLED.SSD1306_EXTERNALVCC:
      self.command(0x22)
    else:
      self.command(0xF1)
    self.command(OLED.SSD1306_SETVCOMDETECT)                 # 0xDB
    self.command(0x40)
    self.command(OLED.SSD1306_DISPLAYALLON_RESUME)           # 0xA4
    self.command(OLED.SSD1306_NORMALDISPLAY)                 # 0xA6

  def command(self, c):
    # I2C write.
    control = 0x00   # Co = 0, DC = 0
    self.pi.i2c_write_byte_data(self._hw, control, c)

  def image(self, image):
    """Set buffer to value of Python Imaging Library image.  The image should
      be in 1 bit mode and a size equal to the display size.
    """
    if image.mode != '1':
      raise ValueError('Image must be in mode 1.')
    imwidth, imheight = image.size
    print("imwidth, imheight", imwidth, imheight)
    if imwidth != self.width or imheight != self.height:
      raise ValueError('Image must be same dimensions as display ({0}x{1}).' \
            .format(self.width, self.height))
    # Grab all the pixels from the image, faster than getpixel.
    pix = image.load()
    # Iterate through the memory pages
    index = 0
    for page in range(self._pages):
      # Iterate through all x axis columns.
      for x in range(self.width):
        # Set the bits for the column of pixels at the current position.
        bits = 0
        # Don't use range here as it's a bit slow
        for bit in [0, 1, 2, 3, 4, 5, 6, 7]:
          bits = bits << 1
          bits |= 0 if pix[(x, page*8+7-bit)] == 0 else 1
        # Update buffer byte and increment to next byte.
        self._buffer[index] = bits
        index += 1

  def clear(self):
    """Clear contents of image buffer."""
    self._buffer = [0]*(self.width*self._pages)

  def display(self):
    """Write display buffer to physical display."""
    self.command(OLED.SSD1306_COLUMNADDR)
    self.command(0)              # Column start address. (0 = reset)
    self.command(self.width-1)   # Column end address.
    self.command(OLED.SSD1306_PAGEADDR)
    self.command(0)              # Page start address. (0 = reset)
    self.command(self._pages-1)  # Page end address.

    for i in range(0, len(self._buffer), 16):
      control = 0x40   # Co = 0, DC = 0
      # print("Index=", i, " Len=", len(self._buffer[i:i+16]))
      # print(self._buffer[i:i+16])
      self.pi.i2c_write_i2c_block_data(self._hw, control, self._buffer[i:i+16])

  def scroll(self):
    self.command(OLED.SSD1306_DEACTIVATE_SCROLL)
    self.command(OLED.SSD1306_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL)
    self.command(0x00)
    self.command(0x07)
    self.command(0x00)
    self.command(0x08)
    self.command(0x01)
    self.command(OLED.SSD1306_ACTIVATE_SCROLL)

  def scroll_stop(self):
    self.command(OLED.SSD1306_DEACTIVATE_SCROLL)
    
if __name__ == '__main__':
  control = DisplayController(Oled.SSD1306_I2C_ADDRESS)
